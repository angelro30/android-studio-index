package com.example.androidstudioindex;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ImcActivity extends AppCompatActivity {
    private Button btnCalcular,btnLimpiar,btnCerrar;
    private EditText txtAltura, txtPeso;
    private TextView lblResultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imc);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        initComponents();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validación de campos vacíos
                if(txtPeso.getText().toString().matches("") || txtAltura.getText().toString().matches("")){
                    Toast.makeText(ImcActivity.this, "Faltan datos",Toast.LENGTH_SHORT).show();
                } else{
                    float peso = Float.parseFloat(txtPeso.getText().toString());
                    float altura = Float.parseFloat(txtAltura.getText().toString()) / 100;
                    if (peso < 0 || altura < 0) {
                        Toast.makeText(ImcActivity.this, "Datos invalidos", Toast.LENGTH_SHORT).show();
                    } else {
                        float imc = peso / (altura * altura);
                        lblResultado.setText(String.format("TU IMC ES: %.2f", imc));
                    }
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtPeso.setText("");
                lblResultado.setText("TU IMC ES:");
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }
    private void initComponents(){
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
    }
}