package com.example.androidstudioindex;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class GradosActivity extends AppCompatActivity {
    private EditText txtGrados;
    private RadioGroup radioGroup;
    private RadioButton radioCtoF, radioFtoC;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private TextView lblResultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grados);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        initComponents();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strGrados = txtGrados.getText().toString().trim();
                if (strGrados.isEmpty()) {
                    Toast.makeText(GradosActivity.this, "Por favor ingresa un valor.", Toast.LENGTH_SHORT).show();
                    return;
                }
                double grados = Double.parseDouble(strGrados);
                if (radioCtoF.isChecked()) {
                    double resultado = (grados * 9/5) + 32;
                    lblResultado.setText(String.format("Resultado: %.2f °F", resultado));
                } else if (radioFtoC.isChecked()) {
                    double resultado = (grados - 32) * 5/9;
                    lblResultado.setText(String.format("Resultado: %.2f °C", resultado));
                } else {
                    Toast.makeText(GradosActivity.this, "Por favor selecciona una opción.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblResultado.setText("Resultado: ");
                txtGrados.setText("");
                txtGrados.requestFocus();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }

    private void initComponents(){
        txtGrados = findViewById(R.id.txtGrados);
        radioGroup = findViewById(R.id.radioGroup);
        radioCtoF = findViewById(R.id.CtoF);
        radioFtoC = findViewById(R.id.FtoC);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        lblResultado = findViewById(R.id.lblResultado);
    }
}