package com.example.androidstudioindex;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CambioDeMonedaActivity extends AppCompatActivity {
    private TextView lblResultado;
    private EditText txtPesos;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cambio_de_moneda);

        initComponents();

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Spinner spinner = findViewById(R.id.spinner);
        String[] opciones = {"Dolares Americanos", "Euros", "Dolar Canadiense", "Libra Esterlina"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(CambioDeMonedaActivity.this, android.R.layout.simple_spinner_item, opciones);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtPesos.getText().toString().matches("")){
                    Toast.makeText(CambioDeMonedaActivity.this, "Faltan datos",Toast.LENGTH_SHORT).show();
                } else{
                    String monedaSeleccionada = spinner.getSelectedItem().toString();
                    double valorDeMoneda = (monedaSeleccionada.matches("Dolares Americanos")) ? 0.06 : (monedaSeleccionada.matches("Euros")) ? 0.055 : (monedaSeleccionada.matches("Dolar Canadiense")) ? 0.082 : 0.047;
                    double resultado = valorDeMoneda * Double.parseDouble(txtPesos.getText().toString());
                    lblResultado.setText(String.format("Total: %.2f", resultado));
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPesos.setText("");
                lblResultado.setText("Total:");
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }
    private void initComponents(){
        btnCalcular  = (Button) findViewById(R.id.btnCalcular);
        btnCerrar    = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar   = (Button) findViewById(R.id.btnLimpiar);
        txtPesos     = (EditText) findViewById(R.id.txtPesos);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
    }
}