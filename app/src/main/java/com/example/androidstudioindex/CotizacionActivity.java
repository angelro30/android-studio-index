package com.example.androidstudioindex;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionActivity extends AppCompatActivity {
    private TextView lblNombre, lblFolio, lblPagoInicial, lblPagoMensual;
    private EditText txtDescripcion, txtValorAuto, txtPorcentaje;
    private RadioButton rdb12, rdb18, rdb24, rdb36, rdb48;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private Cotizacion cotizacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        initComponents();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtDescripcion.getText().toString().matches("") || txtPorcentaje.getText().toString().matches("") || txtValorAuto.getText().toString().matches(""))
                    Toast.makeText(CotizacionActivity.this, "Faltaron datos por ingresar", Toast.LENGTH_SHORT).show();
                else if (Float.parseFloat(txtPorcentaje.getText().toString()) <= 0 || Float.parseFloat(txtPorcentaje.getText().toString()) >= 100)
                    Toast.makeText(CotizacionActivity.this, "Ingrese un porcentaje valido", Toast.LENGTH_SHORT).show();
                else{
                    int plazo = 0;
                    float enganche = 0.0f;
                    float pagoMensual = 0.0f;
                    plazo = rdb12.isChecked() ? 12 : rdb24.isChecked() ? 24 : rdb36.isChecked() ? 36 : 48;
                    cotizacion.setDescripcion(txtDescripcion.getText().toString());
                    cotizacion.setValorAuto(Float.parseFloat(txtValorAuto.getText().toString()));
                    cotizacion.setPorEnganche(Float.parseFloat(txtPorcentaje.getText().toString()));
                    cotizacion.setPlazos(plazo);
                    enganche = cotizacion.calcularPagoInicial();
                    pagoMensual = cotizacion.calcularPagoMensual();
                    lblPagoInicial.setText(String.format("Pago inicial: $%.2f", enganche));
                    lblPagoMensual.setText(String.format("Pago mensual: $%.2f", pagoMensual));
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescripcion.setText("");
                txtValorAuto.setText("");
                txtPorcentaje.setText("");
                lblPagoInicial.setText("Pago inicial: $");
                lblPagoMensual.setText("Pago Mensual: $");
                rdb12.setSelected(true);
                txtDescripcion.setSelected(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initComponents(){
        lblNombre = findViewById(R.id.lblUsuario);
        lblFolio = findViewById(R.id.lblFolio);
        lblPagoInicial= findViewById(R.id.lblPagoInicial);
        lblPagoMensual = findViewById(R.id.lblPagoMensual);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValor);
        txtPorcentaje = findViewById(R.id.txtPorPagoInicial);
        rdb12 = findViewById(R.id.rdb12);
        rdb24 = findViewById(R.id.rdb24);
        rdb36 = findViewById(R.id.rdb36);
        rdb48 = findViewById(R.id.rdb48);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnCerrar =  findViewById(R.id.btnCerrar);
        btnLimpiar =  findViewById(R.id.btnLimpiar);
        cotizacion = new Cotizacion();
        lblFolio.setText("Folio: " + String.valueOf(cotizacion.generarId()));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        lblNombre.setText("Usuario: " + nombre);
        rdb12.setSelected(true);

    }
}