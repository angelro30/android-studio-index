package com.example.androidstudioindex;

import java.io.Serializable;
import java.util.Random;

public class Cotizacion {
    private int folio, plazos;
    private String descripcion;
    private float valorAuto, porEnganche;

    public Cotizacion(int folio, int plazos, String descripcion, float valorAuto, float porEnganche) {
        this.folio = folio;
        this.plazos = plazos;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
    }

    public Cotizacion() {
        this.folio = 0;
        this.plazos = 0;
        this.descripcion = "";
        this.valorAuto = 0.0f;
        this.porEnganche = 0.0f;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    //método de comportamiento
    public int generarId() {
        Random r = new Random();
        return r.nextInt(1000);
    }

    public float calcularPagoInicial(){
        return this.valorAuto * (this.porEnganche/100);
    }
    public float calcularPagoMensual(){
        return (this.valorAuto - this.calcularPagoInicial())/this.plazos;
    }

}
