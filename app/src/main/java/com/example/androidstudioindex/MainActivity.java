package com.example.androidstudioindex;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private EditText txtSaludo;
    private TextView lblSaludo;
    private Button btnSaludo, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        initComponents();
        btnSaludo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = txtSaludo.getText().toString();
                if(text.matches("")){
                    Toast.makeText(MainActivity.this, "Ingrese texto",Toast.LENGTH_SHORT).show();
                } else {
                    lblSaludo.setText(text);
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblSaludo.setText("");
                txtSaludo.setText("");
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }

    private void initComponents(){
        lblSaludo = (TextView)findViewById(R.id.lblSaludo);
        txtSaludo = (EditText)findViewById(R.id.txtNombre);
        btnSaludo = (Button)findViewById(R.id.btnPulsame);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnCerrar = (Button)findViewById(R.id.btnCerrar);
    }

}