package com.example.androidstudioindex;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionIngresoActivity extends AppCompatActivity {
    private EditText txtUsuario;
    private Button btnIngresar, btnRegresar;
    private Cotizacion cotizacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion_ingreso);
        initComponents();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().matches(""))
                    Toast.makeText(CotizacionIngresoActivity.this, "Ingrese el nombre", Toast.LENGTH_SHORT).show();
                else{
                    String nombre = txtUsuario.getText().toString();
                    Intent intent = new Intent(getApplicationContext(), CotizacionActivity.class);
                    intent.putExtra("cliente", nombre);
                    startActivity(intent);
                }
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initComponents(){
        txtUsuario = findViewById(R.id.txtUsuario);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnRegresar = findViewById(R.id.btnRegresar);
        cotizacion = new Cotizacion();
    }
}